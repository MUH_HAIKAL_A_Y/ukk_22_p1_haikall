<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">NAVIGASI UTAMA</li>
          <li class="active treeview">
            <a href="#">
              <i class="fa fa-edit"></i> <span>Catatan Perjalanan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="../perjalanan/"><i class="fa fa-circle-o"></i>Data Perjalanan</a></li>
            </ul>
          </li>
          <li>
            <a href="../logout.php">
              <i class="fa fa-power-off"></i><span>Keluar</span>
            </a>
          </li>
        </ul>
      </section>
      <!-- /.sidebar -->
</aside>