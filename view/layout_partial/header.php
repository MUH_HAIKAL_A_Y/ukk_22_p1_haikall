
<header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="../../index2.html" class="navbar-brand"><b>Peduli</b>Diri</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
          
        

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Catatan Perjalanan<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="../perjalanan/">Data Perjalanan</a></li>
                  <!-- <li><a href="../logout.php"> <i class="fa fa-power-off"></i>  <b>Keluar</b></li> -->
                </ul>
            
            </li>

            <li><a href="../logout.php" class="btn btn-success">logout</a></li>
          </ul>
          
        </div>
         
  


        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
     
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>